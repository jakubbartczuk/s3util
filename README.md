# s3intel.py
Gathers statistics about the content of S3 buckets

Statistics gathered:

- Total data size
- Total file count
- min,max,mean,median of all files
- File extensions found
- min,max,mean,median,total,count broken down by file extension

## Usage

`$> git clone git@bitbucket.org:MattEllsworth/s3util.git`

	$> ./s3intel.py -h
	usage: s3intel.py [-h] --profile PROFILE --region REGION --buckets BUCKETS [BUCKETS ...]
	
	optional arguments:
	  -h, --help            			 show this help message and exit
	  --profile PROFILE     			 ~/.aws/config profile to use
	  --region REGION       			 AWS region to use
	  --buckets BUCKETS [BUCKETS ...]    buckets to target

`$> ./s3intel.py --profile my-aws-profile --region us-west-2 --buckets bucket1 bucket2 ...`

## Example output

	=======================================================
	File Type Results:
	=======================================================
	File Types: dict_keys(['log', 'txt', 'pdf', 'html'])
	=======================================================
	File Type: pdf
	Total Size: 616  /  616.0 bytes
	File Count: 1
	min: 616  /  616.0 bytes
	max: 616  /  616.0 bytes
	median: 616  /  616.0 bytes
	avg: 616  /  616.0 bytes
	Files under 1KB: 1
	Files 1KB to 1MB: 0
	Files 1MB to 1GB: 0
	Files over 1GB: 0
	=======================================================
	File Type: html
	Total Size: 475819632  /  453.8 MB
	File Count: 1
	min: 475819632  /  453.8 MB
	max: 475819632  /  453.8 MB
	median: 475819632  /  453.8 MB
	avg: 475819632  /  453.8 MB
	Files under 1KB: 0
	Files 1KB to 1MB: 0
	Files 1MB to 1GB: 1
	Files over 1GB: 0
	=======================================================
	Full Data Set Results:
	=======================================================
	Total Size: 2547993796508  /  2.3 TB
	File Count: 1111052
	min: 2  /  2.0 bytes
	max: 111543673140  /  103.9 GB
	median: 41472.0  /  40.5 KB
	avg: 2293316.4212908125  /  2.2 MB
	Files under 1KB: 111031
	Files 1KB to 1MB: 855385
	Files 1MB to 1GB: 144451
	Files over 1GB: 185